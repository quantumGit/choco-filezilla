$ErrorActionPreference = 'Stop'

$packageName = 'filezilla'
$url64 = "https://dl1.cdn.filezilla-project.org/client/FileZilla_3.35.2_win64-setup.exe"
$url = "https://dl1.cdn.filezilla-project.org/client/FileZilla_3.35.2_win32-setup.exe"
$checksum = '48D829267C81219D55A278C2C10B6B96F9C58B58D9D708649657EA9F1B3D2F62'
$checksum64 = '05E1F343D3882992D0B944DD6F2E8F6AF1E696B18D666CA314F87FCC8BB930A2'
$checksumtype = 'sha256'

$packageArgs = @{
    packageName    = $packageName
    fileType       = 'EXE'
    url            = $url
    url64bit       = $url64
    silentArgs     = '/S'
    validExitCodes = @(0, 1223)
    checksumType   = $checksumtype
    checksum       = $checksum
    checksumType64 = $checksumtype
    checksum64     = $checksum64
    softwareName   = 'filezilla*'
}
Install-ChocolateyPackage @packageArgs

Start-Sleep -Seconds 1

Write-Host "Configuring FileZilla to not check for updates automatically..." -ForegroundColor Cyan

# Set Default Machine Settings for FileZilla
if(Test-Path 'C:\Program Files\FileZilla FTP Client\docs\fzdefaults.xml.example'){
  Copy-Item 'C:\Program Files\FileZilla FTP Client\docs\fzdefaults.xml.example' 'C:\Program Files\FileZilla FTP Client\docs\fzdefaults.xml' -Force
}

$xPath = '//FileZilla3/Settings/Setting'
$xmlFile = 'C:\Program Files\FileZilla FTP Client\docs\fzdefaults.xml'
$xml = New-Object -TypeName 'System.Xml.XmlDocument';
$xml.Load($xmlFile);
$node = $xml.SelectNodes($xPath);
$updateSetting = $node.Item(2)
$nodevalue = $updateSetting.'#text'
Write-Verbose ("Checking if updates check is enabled")
 if ($nodevalue -ne '1')
    {
        Write-Verbose "Disabling automatic update checks for FileZilla..."
        $updateSetting.'#text' = '1'
        $xml.Save($xmlFile)
    }

Start-Sleep -Seconds 1

Copy-Item 'C:\Program Files\FileZilla FTP Client\docs\fzdefaults.xml' 'C:\Program Files\FileZilla FTP Client\fzdefaults.xml' -Force
