![FileZilla Icon](https://a.fsdn.com/allura/p/filezilla/icon?1521450649?&w=90)

[![Build status](https://ci.appveyor.com/api/projects/status/oumv7yx6cm4s7u0o?svg=true)](https://ci.appveyor.com/project/hubby2004/choco-filezilla)

# FileZilla

[FileZilla](https://filezilla-project.org/) is a cross-platform FTP, SFTP, and FTPS client with a vast list of features, which supports Windows, Mac OS X, Linux, and more. FileZilla's dynamic tools help you move files between your local machine and your Web site's server effortlessly. For example, Filezilla lets you compare your files with in-directory server files to manage file syncing. You can also tab browse between servers and transfer files to them simultaneously, as well as edit server files on the go. And Filezilla is available in 47 languages worldwide!

## Getting Started

These instructions will help you deploy this chocolatey package and configure it on a windows OS.

### Prerequisites

There are no prequisistes for installing FileZilla. Just make sure Chocolatey is installed on the machine.

### Installing

To install FileZilla, run PowerShell as admin and then type the following command:

```powershell
choco install filezilla
```

The silent install documentation can be found [HERE](https://wiki.filezilla-project.org/Silent_Setup)

### Uninstalling

To uninstall FileZilla, run PowerShell as admin and then type the following command:

```powershell
choco uninstall filezilla
```

## Deployment

The installer will configrue FileZilla to not check for updates by creating a machine-based configuration in the installation directory. In this package, the chocolatey script will look for the example configuration file named `fzdefaults.xml.example` under `C:\Program Files\FileZilla FTP Client\docs`. The script will create a copy in the same folder named `fzdefaults.xml`, then modify the following line:

```xml
<Setting name="Disable update check">0</Setting>
```

To disable update check, the script will switch the value from 0 to 1. The xml node will look like the following line:

```xml
<Setting name="Disable update check">1</Setting>
```

Finally, to apply the setting the script will copy `fzdefaults.xml` to `C:\Program Files\FileZilla FTP Client\`.


## Configuration Recommendations

It is recommended that you disable the update check parameter to make sure the customers don't get that message every time they launch the application. However, make sure you have an update schedule to ensure FileZilla gets patched. This chocolatey package will configure the update check parameter automatically for you. Please see the [deployment section](#deployment)

## License

The software is open source and free for use. For more information about the license, please visit the license page for FileZilla at https://filezilla-project.org/license.php
